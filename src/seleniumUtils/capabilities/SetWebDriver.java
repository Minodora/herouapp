public class SetWebDriver {

    public void startWebDriver() {

        String os = System.getProperty("os.name").toLowerCase();

        if (os.contains("mac")) {
            String filepathtoset_mac = System.getProperty("user.dir") + "/chromedriver";
            System.setProperty("webdriver.chrome.driver", filepathtoset_mac);

        } else {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        }

    }
}